angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.data', {
      url: '/data',
      views: {
        'menuContent': {
          templateUrl: 'templates/data.html',
          controller: 'DataCtrl'
        }
      }
    })
    
    .state('app.pay', {
       url: '/pay/:id',
        views: {
        'menuContent': {
          templateUrl: 'templates/pay.html',
          controller: 'PayCtrl'
        }
      }
    });

  $urlRouterProvider.otherwise('/app/login');
})

.controller('AppCtrl', function($scope,$ionicPopup) {
  $scope.logout = function () {
    var confirmPopup = $ionicPopup.confirm({
     title: 'Logout',
     template: 'Are you sure you want to logout?'
   });

   confirmPopup.then(function(res) {
     if(res) {
        ionic.Platform.exitApp();
     } else {
       console.log('You are not sure');
     }
   });
  }
})

.controller('LoginCtrl', function($scope,$location) {
  $scope.login = function(input) {
    if (input.number === '' || input.password === '') {
      alert('Please enter valid details');
    } else {
      $location.path('/app/data');
    }
  }
})

.controller('DataCtrl', function($scope,$location,$http) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $http.get('js/data.json')
    .then(function(response) {
        $scope.response = response.data;
    });

  $scope.pay = function(id) {
    $location.path('/pay/'+id);
  }
})

.controller('PayCtrl', function($scope,$location,$http) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.groups = [];
  
  $scope.groups = [
    { name: 'Credit Card', id: 1, items: [{ subName: 'SubBubbles1', subId: '1-1' }, { subName: 'SubBubbles2', subId: '1-2' }]},
    { name: 'Debit Card', id: 1, items: [{ subName: 'SubGrup1', subId: '1-1' }, { subName: 'SubGrup1', subId: '1-2' }]},
   ];
  
  
  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
  $scope.pay = function(data) {
     alert('Payment Successful');
     $location.path('/app/data');
  }
})

